# Emilia Manual

[![pipeline status](https://gitlab.com/emilia-system/emilia-manual/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-manual/commits/master)
[![License](https://img.shields.io/badge/license-CCPL-blue)](https://choosealicense.com/licenses/cc-by-4.0/)

This package contains the source for the Emilia Manual in LaTex. If you want to compile it, use make.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Known problems

Sometimes the programs evolve rapidly, to the point there is no time left to update the manual, please report any inconsistencies if you find it.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[CCPL](https://choosealicense.com/licenses/cc-by-4.0/)

# Emilia Documentation pt_BR

Este pacote contém a fonte para o manual da Emilia em LaTex. Caso deseje compilar, use make.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

As vezes os programas evoluem rapidamente, não sobrando tempo para atualizar o manual, favor reportar qualquer inconsistência caso ache.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[CCPL](https://choosealicense.com/licenses/cc-by-4.0/)
